package com.bec.workshop.project.students.controller;

import com.bec.workshop.project.students.domain.Student;
import com.bec.workshop.project.students.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("/students")
@RestController
public class StudentsController {
    @Autowired
    StudentService studentService;

    @RequestMapping("/{id}")
    public ResponseEntity getById(@PathVariable String id) {
        Student student = studentService.getById(id);
        if (student == null) {
            return new ResponseEntity<>("Student not found", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(studentService.getById(id), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity getAllUsers() {
        return new ResponseEntity<>(studentService.getAll(), HttpStatus.OK);
    }

    @RequestMapping(value = "", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity saveUser(@RequestBody @Valid Student student) {
        studentService.save(student);
        return new ResponseEntity<>("Student Info created", HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity updateUser(@PathVariable String id, @RequestBody Student student) {
        if (!student.getStudentId().equals(id)) {
            return new ResponseEntity<String>("Student with given id is not found", HttpStatus.BAD_REQUEST);
        }
        studentService.update(student);
        return new ResponseEntity<>("Student updated successfully", HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteUser(@PathVariable String id) {
        Student student = studentService.getById(id);
        if (student == null) {
            return new ResponseEntity<String>("Student with given id is not found", HttpStatus.BAD_REQUEST);
        }
        studentService.delete(student);
        return new ResponseEntity<>("Student updated successfully", HttpStatus.NO_CONTENT);
    }
}
