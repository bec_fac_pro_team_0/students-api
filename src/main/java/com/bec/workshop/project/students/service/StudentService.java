package com.bec.workshop.project.students.service;

import com.bec.workshop.project.students.domain.Student;
import com.bec.workshop.project.students.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.util.Calendar;
import java.util.List;

@Service
public class StudentService {
    @Autowired
    StudentRepository studentRepository;

    public Student getById(String userId) {
        return studentRepository.findOne(userId);
    }

    public void save(Student student) {
        String id = "Y" + Calendar.getInstance().get(Calendar.YEAR) + String.format("%05d", new SecureRandom().nextInt(10000));
        student.setStudentId(id);
        studentRepository.save(student);
    }

    public void update(Student student) {
        studentRepository.save(student);
    }

    public List<Student> getAll() {
        return studentRepository.findAll();
    }

    public void delete(Student student) {
        studentRepository.delete(student);
    }

}
